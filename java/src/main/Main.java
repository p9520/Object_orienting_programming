package main;

import bank.Bank;
import bank.CardToCardSender;
import bank.card.Card;
import bank.card.CardRepository;
import bank.impl.GazpromBankMoneySender;
import bank.impl.SberBankMoneySender;
import bank.impl.TinkoffBankMoneySender;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        initCards();
        CardToCardSender tinkofSender = new TinkoffBankMoneySender(new BigDecimal(0.2));
        CardToCardSender sberSender = new SberBankMoneySender();
        CardToCardSender gazpromSender = new GazpromBankMoneySender(new BigDecimal(0.1));

        tinkofSender.sendMoneyFromCardToCard("1234", "4321", new BigDecimal(1));
        sberSender.sendMoneyFromCardToCard("4321", "9999", new BigDecimal(1000));
        gazpromSender.sendMoneyFromCardToCard("2222", "1234", new BigDecimal(404));
    }

    /**
     * Метод создаёт карты и добавляет их в хранилище для дальнейшей работы.
     */
    private static void initCards() {
        //Создаём три карты.
        Card tinkoffCard = new Card("1234", Bank.TINKOFF);
        Card sberBankCard = new Card("4321", Bank.SBER);
        Card sberBankCardNumberTwo = new Card("9999", Bank.SBER);
        Card gazpromBankCard = new Card("2222", Bank.GAZPROMBANK);
        //Добавляем на них денег
        tinkoffCard.addMoneyToBalance(new BigDecimal(1000));
        sberBankCard.addMoneyToBalance(new BigDecimal(1000));
        sberBankCardNumberTwo.addMoneyToBalance(new BigDecimal(1000));
        gazpromBankCard.addMoneyToBalance(new BigDecimal(1000));
        //Добавляем карты в хранилище
        CardRepository repository = CardRepository.getCardRepositoryInstance();
        repository.addCard(tinkoffCard);
        repository.addCard(sberBankCard);
        repository.addCard(gazpromBankCard);
        repository.addCard(sberBankCardNumberTwo);
    }
}
