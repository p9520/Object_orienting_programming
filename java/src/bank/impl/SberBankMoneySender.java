package bank.impl;

import bank.AbstractCardToCardSender;
import bank.Bank;
import bank.card.Card;

import java.math.BigDecimal;

/**
 * Реализация отправителя денег с карты на карту банка {@link Bank#SBER}.
 *
 * @author Matveev Alexander
 */
public class SberBankMoneySender extends AbstractCardToCardSender {

    public SberBankMoneySender() {
        super();
        this.feeAmount = new BigDecimal(0.4);
    }

    @Override
    public Bank getBank() {
        return Bank.SBER;
    }

    @Override
    public void sendMoneyFromCardToCard(String cardFrom, String cardTo, BigDecimal amount) {
        System.out.println("Отправка денег через " + Bank.SBER);
        Card cardSender = cardRepository.getCardByNumber(cardFrom);
        if (cardSender == null) {
            System.out.println("Карта " + cardFrom + " не найдена");
            return;
        }

        Card cardRecipient = cardRepository.getCardByNumber(cardTo);
        if (cardRecipient == null) {
            System.out.println("Карта " + cardTo + " не найдена");
            return;
        }

        BigDecimal fee = calculateFee(cardSender, cardRecipient, amount);//Расчёт комиссии
        BigDecimal withdrawAmount = amount.add(fee);//Вычисление суммы списания
        cardSender.withdrawMoneyFromBalance(withdrawAmount);//Списание с карты
        cardRecipient.addMoneyToBalance(amount);//Начисление на карту
        System.out.println("Деньги с карты " + cardSender.getCardNumber() + ". Перечислены на карту " + cardTo);
    }
}
