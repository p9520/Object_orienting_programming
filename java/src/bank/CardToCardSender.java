package bank;

import java.math.BigDecimal;

/**
 * Интерфейс отправителя денег с карты на карту.
 *
 * @author Matveev Alexander
 */
public interface CardToCardSender {

    /**
     * Получение Банка который осуществит перевод.
     *
     * @return {@link Bank}
     */
    Bank getBank();

    /**
     * Метод переводит деньги с карты на карту.
     *
     * @param cardFrom номер карты с которой осуществляется перевод.
     * @param cardTo   номер карты получателя перевода.
     * @param amount   Сумма перевода.
     */
    void sendMoneyFromCardToCard(String cardFrom, String cardTo, BigDecimal amount);
}
