package bank;

/**
 * Перечисление банков.
 *
 * @author Matveev Alexander
 */
public enum Bank {

    TINKOFF,
    SBER,
    GAZPROMBANK
}
