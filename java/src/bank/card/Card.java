package bank.card;

import bank.Bank;

import java.math.BigDecimal;

/**
 * Класс описания банковской карты.
 *
 * @author Matveev Alexander
 */
public class Card {

    /**
     * Номер банковской карты.
     */
    private String cardNumber;

    /**
     * Банк выпустивший карту.
     */
    private Bank cardBank;

    /**
     * Баланс карты.
     */
    private BigDecimal balance;

    /**
     * Констркутор класса.
     *
     * @param cardNumber Номер банковской карты.
     * @param cardBank   Банк выпустивший карту.
     */
    public Card(String cardNumber, Bank cardBank) {
        this.cardNumber = cardNumber;
        this.cardBank = cardBank;
        balance = new BigDecimal(0);
    }

    /**
     * Получение номера карты.
     *
     * @return Номер карты.
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Получение банка выпустившего карту.
     *
     * @return Банк выпустивший карту.
     */
    public Bank getCardBank() {
        return cardBank;
    }

    /**
     * Получение баланса карты.
     *
     * @return Баланс карты.
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Метод зачисляет деньги на карту.
     *
     * @param amount сумма зачисления.
     */
    public void addMoneyToBalance(BigDecimal amount) {
        System.out.println("Зачисляем деньги на карту " + this.cardNumber);
        balance = balance.add(amount);
        System.out.println("Баланс карты = " + this.balance);
    }

    /**
     * Метод списывает деньги с карты.
     *
     * @param amount сумма списания.
     * @return Признак, произошло ли списание.
     */
    public Boolean withdrawMoneyFromBalance(BigDecimal amount) {
        System.out.println("Списываем деньги с карты " + this.cardNumber);
        if (this.balance.compareTo(amount) == -1) {//Если баланс карты меньше суммы списания
            System.out.println("Сумма списания больше" + amount + " баланса карты. Списание невозможно");
            return false;//Списание не произошло. метод возвращает false
        }
        balance = balance.add(amount.negate());
        System.out.println("Баланс карты = " + this.balance);
        return true;//Списание произошло
    }
}
