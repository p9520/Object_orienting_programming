package bank.card;

import java.util.HashMap;
import java.util.Map;

/**
 * Хранилище карт.
 *
 * @author Matveev Alexander
 */
public class CardRepository {

    /**
     * Единственный экземпляр хранилища.
     */
    private static CardRepository instance = new CardRepository();

    /**
     * Коллекция карт.
     */
    private Map<String, Card> cards;

    /**
     * Конструктор класса.
     */
    private CardRepository() {
        cards = new HashMap<>();
    }

    /**
     * Получение хранилища карт.
     *
     * @return {@link CardRepository}.
     */
    public static CardRepository getCardRepositoryInstance() {
        return instance;
    }

    /**
     * Добавление карты в хранилище.
     *
     * @param card {@link Card}.
     */
    public void addCard(Card card) {
        cards.put(card.getCardNumber(), card);
    }

    /**
     * Получение карты из хранилища по номеру карты.
     *
     * @param cardNumber номер карты.
     * @return {@link Card}.
     */
    public Card getCardByNumber(String cardNumber) {
        return cards.get(cardNumber);
    }
}
