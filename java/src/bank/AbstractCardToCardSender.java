package bank;

import bank.card.Card;
import bank.card.CardRepository;

import java.math.BigDecimal;

/**
 * Абстрактная реализация отправителя денег с карты на карту.
 *
 * @author Matveev Alexander
 */
public abstract class AbstractCardToCardSender implements CardToCardSender {

    /**
     * Ставка комиссии в % от суммы платежа.
     */
    protected BigDecimal feeAmount;

    /**
     * {@link CardRepository}.
     */
    protected CardRepository cardRepository;

    public AbstractCardToCardSender() {
        System.out.println("Создаём отправителя денег банка " + this.getBank());
        this.cardRepository = CardRepository.getCardRepositoryInstance();
    }

    /**
     * Метод расчитывает комиссию на перевод.
     *
     * @param cardFrom Карта с которой осуществляется перевод.
     * @param cardTo   Карта на которую будет осуществляться перевод.
     * @param amount   Сумма перевода.
     * @return размер комиссии.
     */
    protected BigDecimal calculateFee(Card cardFrom, Card cardTo, BigDecimal amount) {
        BigDecimal fee = new BigDecimal(0);//итоговый размер комиссии.
        Bank thisBank = getBank();//Получаем значение банка который осуществляет перевод.
        if (!cardFrom.getCardBank().equals(thisBank)) {// Если карта с которой осуществляется перевод не выпущена в банке которые делает перевод
            BigDecimal feeForDifferentBank = amount.multiply(feeAmount);//Размер комиссии если банк выпускающий карту другой.
            fee.add(feeForDifferentBank);//Добавляем к результату комиссию за другой банк
        }
        if (!cardTo.getCardBank().equals(thisBank)) {// Если карта на которую осуществляется перевод не выпущена в банке которые делает перевод
            BigDecimal feeForDifferentBank = amount.multiply(feeAmount);//Размер комиссии если банк выпускающий карту принимающую перевод другой.
            fee.add(feeForDifferentBank);//Добавляем к результату комиссию за другой банк
        }
        return fee;//Возвращаем итоговую комиссию.
    }
}
